# picoinflux dnsdist Stats
===
 gets the json stats endpoint and sends it to influxdb
## Arguments + example

Arguments are positional 
| num | thingy| required | example |
|--|--|--|
| 1 | URL of dnsdist web server     | yes | http://127.0.0.1:8353 | 
| 2 | API_KEY of dnsdist web server| yes | n955bTxwPeVSTY_aMQlNsutR_wMbLkmiXnfbWgaIjrzcJKrk |
| 3 | hostname sent | no | myhost |
```
bash  /etc/custom/picodnsdist/ppicoinflux-dnsdist.sh http://127.0.0.1:8353 MyDNSdistAPIKey 
```
## exported measurements:

* dnsdist_acl_drops
* dnsdist_cache_hits
* dnsdist_cache_misses
* dnsdist_cpu_iowait
* dnsdist_cpu_steal
* dnsdist_cpu_sys_msec
* dnsdist_cpu_user_msec
* dnsdist_doh_query_pipe_full
* dnsdist_doh_response_pipe_full
* dnsdist_downstream_send_errors
* dnsdist_downstream_timeouts
* dnsdist_dyn_block_nmg_size
* dnsdist_dyn_blocked
* dnsdist_empty_queries
* dnsdist_fd_usage
* dnsdist_frontend_noerror
* dnsdist_frontend_nxdomain
* dnsdist_frontend_servfail
* dnsdist_latency_avg100
* dnsdist_latency_avg1000
* dnsdist_latency_avg10000
* dnsdist_latency_avg1000000
* dnsdist_latency_count
* dnsdist_latency_slow
* dnsdist_latency_sum
* dnsdist_latency0_1
* dnsdist_latency1_10
* dnsdist_latency10_50
* dnsdist_latency100_1000
* dnsdist_latency50_100
* dnsdist_no_policy
* dnsdist_noncompliant_queries
* dnsdist_noncompliant_responses
* dnsdist_outgoing_doh_query_pipe_full
* dnsdist_over_capacity_drops
* dnsdist_packetcache_hits
* dnsdist_packetcache_misses
* dnsdist_proxy_protocol_invalid
* dnsdist_queries
* dnsdist_rdqueries
* dnsdist_real_memory_usage
* dnsdist_responses
* dnsdist_rule_drop
* dnsdist_rule_nxdomain
* dnsdist_rule_refused
* dnsdist_rule_servfail
* dnsdist_rule_truncated
* dnsdist_security_status
* dnsdist_self_answered
* dnsdist_server_policy
* dnsdist_servfail_responses
* dnsdist_tcp_cross_protocol_query_pipe_full
* dnsdist_tcp_cross_protocol_response_pipe_full
* dnsdist_tcp_listen_overflows
* dnsdist_tcp_query_pipe_full
* dnsdist_too_old_drops
* dnsdist_trunc_failures
* dnsdist_udp_in_csum_errors
* dnsdist_udp_in_errors
* dnsdist_udp_noport_errors
* dnsdist_udp_recvbuf_errors
* dnsdist_udp_sndbuf_errors
* dnsdist_udp6_in_csum_errors
* dnsdist_udp6_in_errors
* dnsdist_udp6_noport_errors
* dnsdist_udp6_recvbuf_errors
* dnsdist_udp6_sndbuf_errors
* dnsdist_uptime



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
