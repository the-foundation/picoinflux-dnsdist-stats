#!/bin/bash

URL="$1"
APIKEY="$2"

TMPDATABASE=/dev/shm/.picoinfluxdata.dnsdist

#### time stamp and hostname ####
timestamp_nanos() { if [[ $(date -u +%s%N|grep ^[0-9] |wc -c) -eq 20  ]]; then date +%s%N;else expr $(date -u +%s) "*" 1000 "*" 1000 "*" 1000 ; fi ; } ;
hostname=$(cat /etc/picoinfluxid 2>/dev/null || (which hostname >/dev/null && hostname || (which uci >/dev/null && uci show |grep ^system|grep hostname=|cut -d\' -f2 ))) 2>/dev/null

[[ -z "$3" ]] || hostname=$3
dnsdistres=$( curl -H "X-API-Key: $APIKEY" "${URL}/jsonstat?command=stats" -s)
echo "$dnsdistres" | grep -q Unauthorized && echo "ERROR_DNSDIST_FAILED_AUHORIZATION" >&2
echo "$dnsdistres" | grep -q Unauthorized || (
    (
    echo "$dnsdistres" |jq .|grep '"'|sed 's/^ \+//g'|sed 's/"//g;s/: /=/g;s/-/_/g;s/,$//g'|sed 's/^/dnsdist_/g'|grep "[0-9]$"
    ) |grep -v ^$ |grep -v =$| sed  's/\(.*\)=/\1,host='"$hostname"' value=/'|sed  's/$/ '$(timestamp_nanos)'/g'  |grep " value="  |grep -E ' [0-9]{19}$' > "${TMPDATABASE}"
)

test -e "${TMPDATABASE}" || (echo -n > "${TMPDATABASE}" )
##TRANSMISSION STAGE::
##
## shall we use a proxy ?
##grep -q ^PROXYFFLUX= ${HOME}/.picoinflux.conf && export ALL_PROXY=$(grep ^PROXYFFLUX= ${HOME}/.picoinflux.conf|tail -n1 |cut -d= -f2- )

PROXYSTRING=""

##check config presence of secondary host and replicate in that case
grep -q "^SECONDARY=true" ${HOME}/.picoinflux.conf && (
    ( ( test -f ${TMPDATABASE} && cat ${TMPDATABASE} ; test -f ${TMPDATABASE}.secondary && cat ${TMPDATABASE}.secondary ) | sort |uniq > ${TMPDATABASE}.tmp ;
     mv ${TMPDATABASE}.tmp ${TMPDATABASE}.secondary )  ##
    grep -q ^PROXYFLUX_SECONDARY= ${HOME}/.picoinflux.conf && PROXYSTRING='-x '$(grep ^PROXYFLUX_SECONDARY= ${HOME}/.picoinflux.conf|tail -n1 |cut -d= -f2- )
    grep -q "^TOKEN2=true" $HOME/.picoinflux.conf && ( echo using header auth > /dev/shm/picoinflux.dnsdist.send.secondary.log; (curl $PROXYSTRING --retry-delay 30 --retry 3 -v -k --header "Authorization: Token $(grep ^AUTH2= $HOME/.picoinflux.conf|cut -d= -f2-)" -i -XPOST "$(grep ^URL2 ~/.picoinflux.conf|cut -d= -f2-)" --data-binary @${TMPDATABASE}.secondary 2>&1 && rm ${TMPDATABASE}.secondary 2>&1 ) >/tmp/picoinflux.secondary.log  )
    grep -q "^TOKEN2=true" $HOME/.picoinflux.conf || ( echo using passwd auth > /dev/shm/picoinflux.dnsdist.send.secondary.log; (curl $PROXYSTRING --retry-delay 30 --retry 3 -v -k -u $(grep ^AUTH2= $HOME/.picoinflux.conf|cut -d= -f2-) -i -XPOST "$(grep ^URL2 $HOME/.picoinflux.conf|cut -d= -f2-|tr -d '\n')" --data-binary @${TMPDATABASE}.secondary 2>&1 && rm ${TMPDATABASE}.secondary 2>&1 ) & ) >/tmp/picoinflux.secondary.log   )
    grep -q ^PROXYFFLUX= ${HOME}/.picoinflux.conf && PROXYSTRING='-x '$(grep ^PROXYFFLUX= ${HOME}/.picoinflux.conf|tail -n1 |cut -d= -f2- )

grep -q "^TOKEN=true" ~/.picoinflux.conf && (
  (echo using header auth > /dev/shm/picoinflux.dnsdist.send.log;echo "size $(wc -l ${TMPDATABASE}) lines ";curl  $PROXYSTRING --retry-delay 30 --retry 3 -v -k --header "Authorization: Token $(head -n1 $HOME/.picoinflux.conf)" -i -XPOST "$(head -n2 ~/.picoinflux.conf|tail -n1)" --data-binary @${TMPDATABASE} 2>&1 && mv ${TMPDATABASE} /tmp/.influxdata.dnsdist.last 2>&1 ) >/tmp/picoinflux.dnsdist.log  )

grep -q "^TOKEN=true" ~/.picoinflux.conf || ( \
  (echo using passwd auth > /dev/shm/picoinflux.dnsdist.send.log;echo "size $(wc -l ${TMPDATABASE}) lines ";curl  $PROXYSTRING --retry-delay 30 --retry 3 -v -k -u $(head -n1 $HOME/.picoinflux.conf) -i -XPOST "$(head -n2 $HOME/.picoinflux.conf|tail -n1)" --data-binary @${TMPDATABASE} 2>&1 && mv ${TMPDATABASE} /tmp/.influxdata.dnsdist.last 2>&1 ) >/tmp/picoinflux.dnsdist.log  )

#(curl -s -k -u $(head -n1 ~/.picoinflux.conf) -i -XPOST "$(head -n2 ~/.picoinflux.conf|tail -n1)" --data-binary @${TMPDATABASE} 2>&1 && mv ${TMPDATABASE} ${TMPDATABASE}.sent 2>&1 ) >/tmp/picoinflux.dnsdist.log



## picoinflux.conf examples (FIRST LINE OF THE FILE(!!) is the pass/token,second line url URL , rest is ignored except secondary config and socks )
##example V1
#user:buzzword
#https://corlysis.com:8086/write?db=mydatabase



## example V2
#KJAHSKDUHIUHIuh23ISUADHIUH2IUAWDHiojoijasd2asodijawoij12e_asdioj2ASOIDJ3==
#https://eu-central-1-1.aws.cloud2.influxdata.com/api/v2/write?org=deaf13beef12&bucket=sys&precision=ns
#TOKEN=true


### add the following lines for a backup/secondary write with user/pass auth:
# SECONDARY=true
# URL2=https://corlysis.com:8086/write?db=mydatabase
# AUTH2=user:buzzword
# TOKEN2=false
#

##  add the following lines for a backup/secondary write with token (influx v2):
# SECONDARY=true
# URL2=https://eu-central-1-1.aws.cloud2.influxdata.com/api/v2/write?org=deaf13beef12&bucket=sys&precision=ns
# AUTH2=KJAHSKDUHIUHIuh23ISUADHIUH2IUAWDHiojoijasd2asodijawoij12e_asdioj2ASOIDJ3==
# TOKEN2=true
#

### to use socks proxy
#PROXYFFLUX=socks5h://127.0.0.1:9050
